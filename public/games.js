(function() {

    var GameModule = angular.module("GameModule", []);

    var __GameSvc = function($http, $q) {

        this.getAllGames = function () {
            var defer = $q.defer();
            $http.get("/list")
                .then(function (result) {
                    //Good
                    defer.resolve(result.data);
                }).catch(function (error) {
                defer.reject(error);
            });
            return (defer.promise);
        };
        
        this.getGame = function (gameid){
            if(!gameid)
                return ($q.reject("Missing game id"));
            
            var defer = $q.defer();
            $http.get("/game/" + gameid)
                .then(function(result) {
                    defer.resolve(result.data)
                    console.info(result.data);
                }).catch(function(err) {
                defer.reject(err);
            });
            return (defer.promise);
        }
    };
    
    GameModule.service("GameSvc", ["$http", "$q", __GameSvc]);

})();
    
    
        